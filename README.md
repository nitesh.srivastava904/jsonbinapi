**Project Description**

- This project consist of functional automated test cases for CRUD operation of json bin api using REST Assured.
- Details of json bin api have been referred from here:- https://jsonbin.io/api-reference


**Test Scenarios**

- createJsonBinApi (Test Case 1) - Happy scenario for POST operation - Creating a JSON Bin.
- getBin (Test Case 2) - Happy scenario for GET operation - Reading a specific Bin.
- updateJsonBinApi (Test Case 3) - Happy scenario for PUT operation - Updating a specific Bin.
- deleteJsonBinApi (Test Case 4) - Happy scenario for DELETE operation - Deleting a specific Bin.
- getLatestJsonBinApi (Test Case 5) - Happy scenario for GET operation - Reading latest record of a specific Bin.
- invalidRequestPost (Test Case 6) - Error scenario for POST operation - If no text is passed in "sample" key of request body.
- invalidUrlPost (Test Case 7) - Error scenario for POST operation - If the request url is not correct.
- invalidUrlGet (Test Case 8) - Error scenario for GET operation - If the request url is not correct.
- getWithoutId (Test Cace 9) - Error scenario for GET operation - If mandatory path parameter is not provided in request url.
- invalidRequestPut (Test Case 10) - Error scenario for PUT operation - If no text is passed in "sample" key of request body.
- invalidUrlPut (Test Case 11) - Error scenario for PUT operation - If the request url is not correct.
- putWithoutId (Test Case 12) - Error scenario for PUT operation - If mandatory path parameter is not provided in request url.
- invalidUrlDelete (Test Case 13) - Error scenario for DELETE operation - If the request url is not correct.
- deleteWithoutId (Test Case 14) - Error scenario for DELETE operation - If mandatory path parameter is not provided in request url.


**Prerequisite Software**


- Java
- Any Java IDE (Intellij, Eclipse etc)


**How to execute the tests**

- Clone the project.
- Open the project in Intellij or Eclipse.
- Run tests.


**About the project structure**

src/test/java


- JsonBinConfig - It has all the prerequisite configuration which is common for all tests.
- JsonBinEndpoints - It has all the endpoints of json bin api.
- JsonBinTests - It has all the test cases.


**Things which can be added**

- More error scenarios can be implemented.
- Integration into CI/CD pipeline.
- Log4J implementation for reporting.
