package config;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.junit.BeforeClass;

public class JsonBinConfig {

    public static RequestSpecification jsonBin_requestSpec;
    public static ResponseSpecification jsonBin_responseSpec;

    @BeforeClass
    public static void setup() {
        jsonBin_requestSpec = new RequestSpecBuilder()
                .setBaseUri("https://api.jsonbin.io")
                .setBasePath("/v3")
                .addHeader("Content-Type", "application/json")
                .addHeader("X-Master-key", "$2b$10$Ru0pLUZtNvikQxArJZTxk.M7BbBzW9j8QRnyvFi6PBY9HT3Z8HLz6")
                .addFilter(new RequestLoggingFilter())
                .addFilter(new ResponseLoggingFilter())
                .build();

        jsonBin_responseSpec = new ResponseSpecBuilder()
                .build();

        RestAssured.requestSpecification = jsonBin_requestSpec;
        RestAssured.responseSpecification = jsonBin_responseSpec;
    }
}
