package config;

public interface JsonBinEndpoints {

    String CREATE_JSON_BIN = "/b";
    String GET_JSON_BIN = "/b/{Id}";
    String LATEST_JSON_BIN = "/b/{Id}/latest";
    String ERROR_JSON_BIN = "";
    String ERROR_GET_BIN = "/{Id}";
    String ERROR_WITHOUT_ID = "/b";
}
