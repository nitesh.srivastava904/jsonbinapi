import config.JsonBinConfig;
import config.JsonBinEndpoints;
import io.restassured.response.Response;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class JsonBinTests extends JsonBinConfig {

    private static String id;
    private static String var = "Hello World";

    //Happy Scenarios
    @Test
    //Happy Scenario Post
    public void createJsonBinApi() {
        String jsonBinBody = "{\n" +
                "   \"sample\":\"Hello World\"\n" +
                "}";
        given()
                .body(jsonBinBody).
        when()
                .post(JsonBinEndpoints.CREATE_JSON_BIN).
        then()
                .statusCode(200)
                .body("record.sample", equalTo(var))
                .body("metadata.id",notNullValue())
                .body("metadata.createdAt", notNullValue())
                .body("metadata.private", equalTo(true));
    }

    @BeforeClass
    //PreRequisite to set ID for all requests
    public static void extractId() {
        String jsonBinBody = "{\n" +
                "   \"sample\":\"Hello World\"\n" +
                "}";
        Response response =
        given()
                .body(jsonBinBody).
        when()
                .post(JsonBinEndpoints.CREATE_JSON_BIN).
        then()
                .statusCode(200)
                .extract().response();
        id = response.path("metadata.id");
        System.out.println(id);
    }

    @Test
    //Happy Scenario Get
    public void getBin() {

        given()
                .pathParam("Id",id).
        when()
                .get(JsonBinEndpoints.GET_JSON_BIN).
        then()
                .statusCode(200)
                .body("record.sample", equalTo(var))
                .body("metadata.id", equalTo(id))
                .body("metadata.private", equalTo(true));
    }

    @Test
    //Happy Scenario Update
    public void updateJsonBinApi(){
        String jsonBinBody = "{\n" +
                "   \"sample\":\"Hello World\"\n" +
                "}";
        given()
                .pathParam("Id", id)
                .body(jsonBinBody).
        when()
                .put(JsonBinEndpoints.GET_JSON_BIN).
        then()
                .statusCode(200)
                .body("record.sample", equalTo(var))
                .body("metadata.parentId", equalTo(id))
                .body("metadata.private", equalTo(true));
    }

    @AfterClass
    //Happy Scenario Delete
    public static void deleteJsonBinApi(){
        given()
                .pathParam("Id", id).
        when()
                .delete(JsonBinEndpoints.GET_JSON_BIN).
        then()
                .statusCode(200)
                .body("metadata.id", equalTo(id))
                .body("metadata.versionsDeleted", equalTo(0))
                .body("message", equalTo("Bin deleted successfully"));
    }

    @Test
    //Reading latest record of a bin
    public void getLatestJsonBinApi(){
        given()
                .pathParam("Id", id).
        when()
                .get(JsonBinEndpoints.LATEST_JSON_BIN).
        then()
                .statusCode(200)
                .body("record.sample", equalTo(var))
                .body("metadata.id", equalTo(id))
                .body("metadata.private", equalTo(true))
                .body("metadata.createdAt", notNullValue());
    }

    //Error Scenarios
    @Test
    //Invalid request body post
    public void invalidRequestPost(){
        String jsonBinBody = "{\n" +
                "   \"sample\":\n" +
                "}";
        given()
                .body(jsonBinBody).
        when()
                .post(JsonBinEndpoints.CREATE_JSON_BIN).
        then()
                .statusCode(400)
                .body("message", equalTo("Invalid JSON. Please try again"));
    }

    @Test
    //Invalid url post
    public void invalidUrlPost(){
        String jsonBinBody = "{\n" +
                "   \"sample\":\"Hello World\"\n" +
                "}";
        given()
                .body(jsonBinBody).
        when()
                .post(JsonBinEndpoints.ERROR_JSON_BIN).
        then()
                .statusCode(404);
    }

    @Test
    //Invalid url get
    public void invalidUrlGet(){
        given()
                .pathParam("Id", id).
        when()
                .get(JsonBinEndpoints.ERROR_GET_BIN).
        then()
                .statusCode(404)
                .body("message", equalTo("Route not found!"));
    }

    @Test
    //Get bin without id
    public void getWithoutId(){
        given().
        when()
                .get(JsonBinEndpoints.ERROR_WITHOUT_ID).
        then()
                .statusCode(404)
                .body("message", equalTo("Route not found!"));
    }

    @Test
    //Invalid request body put
    public void invalidRequestPut() {
        String jsonBinBody = "{\n" +
                "   \"sample\":\n" +
                "}";
        given()
                .body(jsonBinBody)
                .pathParam("Id", id).
                when()
                .put(JsonBinEndpoints.GET_JSON_BIN).
                then()
                .statusCode(400)
                .body("message", equalTo("Invalid JSON. Please try again"));
    }

    @Test
    //Invalid url put
    public void invalidUrlPut(){
        String jsonBinBody = "{\n" +
                "   \"sample\":\"Hello World\"\n" +
                "}";
        given()
                .body(jsonBinBody)
                .pathParam("Id", id).
        when()
                .put(JsonBinEndpoints.ERROR_GET_BIN).
        then()
                .statusCode(404);
    }

    @Test
    //Put request without id
    public void putWithoutId(){
        String jsonBinBody = "{\n" +
                "   \"sample\":\"Hello World\"\n" +
                "}";
        given()
                .body(jsonBinBody).
        when()
                .put(JsonBinEndpoints.ERROR_WITHOUT_ID).
        then()
                .statusCode(404);
    }

    @Test
    //Invalid url delete
    public void invalidUrlDelete(){
        given()
                .pathParam("Id", id).
        when()
                .delete(JsonBinEndpoints.ERROR_GET_BIN).
        then()
                .statusCode(404);
    }

    @Test
    //Delete request without id
    public void deleteWithoutId(){
        given().
        when()
                .delete(JsonBinEndpoints.ERROR_WITHOUT_ID).
        then()
                .statusCode(404);
    }
}

